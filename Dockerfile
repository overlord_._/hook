FROM python:3.10-slim

ENV PYTHONUNBUFFERED 1

RUN pip install pip==21.3.1
RUN pip install docker-compose==1.29.2 && pip install poetry==1.4.2 && poetry config virtualenvs.create false

WORKDIR /app
COPY pyproject.toml .
COPY poetry.lock .
RUN poetry install --no-root

COPY . .

EXPOSE 8000

CMD python manage.py migrate && gunicorn --bind 0.0.0.0:8000 hook_project.wsgi
