from typing import List

from django.db import connection
from django.db.models import QuerySet, Q

from roulette.models import Round, RoundState, UserMove


def dictfetchall(cursor) -> List[dict]:
    """
        Collection of raw SQL objects to list of dicts
    """
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def get_rounds() -> QuerySet[Round]:
    """
        Get all rounds from DB
    """
    return Round.objects.all()


def check_if_user_is_in_round(user_id: int, round_id: int) -> Round:
    """
        Check if user participates in round
    """
    return Round.objects.filter(Q(id=round_id) & Q(users__id=user_id)).prefetch_related("states").first()


def get_round_last_state(round: Round) -> RoundState:
    """
        Get round last state
    """
    return round.states.order_by("-id").first()


def create_round_state(round: Round, available_numbers: List[int], drawn_numbers: List[int]) -> RoundState:
    """
        Create round new state
    """
    new_state = RoundState.objects.create(
        round=round,
        available_numbers=available_numbers,
        drawn_numbers=drawn_numbers
    )
    return new_state


def create_user_move(round: Round, new_state: RoundState, user_id: int, picked_number: int):
    """
        Create new user move
    """
    UserMove.objects.create(
        round=round,
        state=new_state,
        user_id=user_id,
        number=picked_number
    )


def calculate_online_statistic() -> List[dict]:
    """
        Calculates roulette online by counting users in each round
    """
    with connection.cursor() as cursor:
        cursor.execute(
            """
                select r.id as round_id, count(round_id) as users_count from rounds r 
                    join rounds_users ru on r.id = ru.round_id group by r.id order by r.id;
            """
        )
        return dictfetchall(cursor)


def get_most_active_users() -> List[dict]:
    """
        Gets most active users in roulette by selecting users with average spin count higher than average spin count mean
    """
    with connection.cursor() as cursor:
        cursor.execute(
            """
                with main_data as (select rcnt.*, count(um.user_id) / rcnt.round_count::numeric as average_spin_count from
                   (select u.id as user_id, count(ru.user_id) as round_count from auth_user u
                        join rounds_users ru on u.id = ru.user_id
                    group by (u.id, ru.user_id)
                    ) as rcnt left join user_moves um on rcnt.user_id = um.user_id
                group by (rcnt.user_id, rcnt.round_count))
                select * from main_data where average_spin_count > (select avg(average_spin_count) from main_data);
            """
        )
        return dictfetchall(cursor)
