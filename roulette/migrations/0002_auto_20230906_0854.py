# Generated by Django 4.1.11 on 2023-09-06 05:54

from django.db import migrations
from django.contrib.auth.models import User

from roulette.models import Round, RoundState


def init_database(apps, schema_editor):
    for i in range(1, 7):
        User.objects.create_user(f"user{i}", password="123")

    first_round = Round.objects.create(
        status=Round.RoundStatus.ONGOING,
    )

    RoundState.objects.create(
        round=first_round,
    )


class Migration(migrations.Migration):

    dependencies = [
        ('roulette', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(init_database)
    ]
