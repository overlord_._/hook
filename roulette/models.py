from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

from django.utils.translation import gettext_lazy as _


class AbstractBaseModel(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        abstract = True


class Round(AbstractBaseModel):
    class RoundStatus(models.TextChoices):
        ENDED = "ended", _("ended")
        ONGOING = "ongoing", _("ongoing")

    users = models.ManyToManyField(User, related_name="rounds", verbose_name="users in round")
    status = models.CharField(
        max_length=20,
        choices=RoundStatus.choices,
        default=RoundStatus.ONGOING,
        verbose_name="status",
    )

    class Meta:
        db_table = "rounds"


class RoundState(AbstractBaseModel):
    round = models.ForeignKey(
        Round,
        on_delete=models.CASCADE,
        related_name="states",
        verbose_name="round",
    )

    available_numbers = ArrayField(
        models.IntegerField(),
        default=list(settings.DEFAULT_AVAILABLE_NUMBERS),
        verbose_name="available numbers"
    )

    drawn_numbers = ArrayField(
        models.IntegerField(),
        default=list(),
        verbose_name="drawn numbers"
    )

    class Meta:
        db_table = "round_states"


class UserMove(AbstractBaseModel):
    round = models.ForeignKey(
        Round,
        on_delete=models.CASCADE,
        related_name="moves",
        verbose_name="round"
    )

    state = models.OneToOneField(
        RoundState,
        on_delete=models.CASCADE,
        related_name="move",
        verbose_name="state"
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="moves",
        verbose_name="user"
    )

    number = models.IntegerField(verbose_name="dropped number")

    class Meta:
        db_table = "user_moves"
