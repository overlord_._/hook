from django.contrib.auth.models import User
from rest_framework import serializers

from roulette.models import Round, RoundState, UserMove


class UserSerializer(serializers.ModelSerializer):
    """ User model serializer """

    class Meta:
        model = User
        fields = "__all__"


class RoundStateSerializer(serializers.ModelSerializer):
    """ RoundState model serializer """
    class Meta:
        model = RoundState
        fields = "__all__"


class UserMoveSerializer(serializers.ModelSerializer):
    """ UserMove model serializer """
    class Meta:
        model = UserMove
        fields = "__all__"


class RoundListSerializer(serializers.ModelSerializer):
    """ Round model serializer for reading data """
    users = UserSerializer(many=True)
    states = RoundStateSerializer(many=True)
    moves = UserMoveSerializer(many=True)

    class Meta:
        model = Round
        fields = "__all__"


class RoundWriteSerializer(serializers.ModelSerializer):
    """ Round model serializer for writing data """
    users = serializers.PrimaryKeyRelatedField(many=True, queryset=User.objects.all())

    def create(self, validated_data):
        users = validated_data.pop("users")
        round = Round.objects.create(**validated_data)
        if users:
            round.users.add(*users)
        RoundState.objects.create(round=round)
        return round

    def update(self, instance, validated_data):
        users = validated_data.pop("users")
        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.users.clear()
        instance.users.add(*users)
        instance.save()
        return instance

    class Meta:
        model = Round
        fields = ["id", "status", "users"]


class SpinnedRouletteSerializer(serializers.Serializer):
    """ Serializer for spin roulette response"""
    picked_number = serializers.IntegerField(required=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass
