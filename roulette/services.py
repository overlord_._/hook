import random
from typing import Union

from roulette.db_services import check_if_user_is_in_round, get_round_last_state, create_round_state, create_user_move
from roulette.models import Round


def spin_roulette(user_id: int, round_id: int) -> Union[dict, int]:
    """
        Handles spin roulette request
    """
    round = check_if_user_is_in_round(user_id=user_id, round_id=round_id)
    if not round or round.status == Round.RoundStatus.ENDED:
        return 400

    round_last_state = get_round_last_state(round=round)

    available_numbers = round_last_state.available_numbers
    if available_numbers == [0]:
        picked_number = 0
        round.status = Round.RoundStatus.ENDED
        round.save()
    else:
        picked_number = random.choice([n for n in available_numbers if n != 0])

    available_numbers.remove(picked_number)
    drawn_numbers = round_last_state.drawn_numbers + [picked_number]

    new_state = create_round_state(round=round, available_numbers=available_numbers, drawn_numbers=drawn_numbers)
    create_user_move(round=round, new_state=new_state, user_id=user_id, picked_number=picked_number)

    return {"picked_number": picked_number}
