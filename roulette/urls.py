from django.urls import path
from rest_framework.routers import SimpleRouter

from roulette.views import (
    RoundView,
    SpinRouletteView,
    RouletteOnlineStatisticView,
    MostActiveUsersView,
)

router = SimpleRouter()

router.register("rounds", RoundView, "rounds")

urlpatterns = [
    *router.urls,
    path("roulette/spin/", SpinRouletteView.as_view(), name="spin_roulette"),
    path("roulette/online/", RouletteOnlineStatisticView.as_view(), name="get_roulette_online"),
    path("roulette/active_users/", MostActiveUsersView.as_view(), name="get_active_users"),
]
