from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.openapi import Schema
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from roulette.db_services import get_rounds, calculate_online_statistic, get_most_active_users
from roulette.serializers import RoundListSerializer, RoundWriteSerializer, SpinnedRouletteSerializer
from roulette.services import spin_roulette


@method_decorator(name="list", decorator=swagger_auto_schema(operation_summary="Returns rounds list"))
@method_decorator(name="create", decorator=swagger_auto_schema(operation_summary="Create round"))
@method_decorator(name="retrieve", decorator=swagger_auto_schema(operation_summary="Returns certain round"))
@method_decorator(name="destroy", decorator=swagger_auto_schema(operation_summary="Deletes certain round"))
@method_decorator(name="partial_update", decorator=swagger_auto_schema(operation_summary="Partially updates round"))
@method_decorator(name="update", decorator=swagger_auto_schema(operation_summary="Fully updates round"))
class RoundView(
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet
):
    queryset = get_rounds()

    def get_serializer_class(self):
        if self.action in {"retrieve", "list"}:
            return RoundListSerializer
        return RoundWriteSerializer

    def partial_update(self, request, *args, **kwargs):
        round = self.get_object()
        existing_users = round.users.all()
        serializer = RoundWriteSerializer(
            round, data=request.data, partial=True, context=self.get_serializer_context()
        )
        if serializer.is_valid():
            users = request.data.get("users", None)
            if users:
                round.users.add(*existing_users, *users)
            round = serializer.save(users=[*existing_users, *users])
            round_serializer = RoundListSerializer(round, context=self.get_serializer_context())
            return Response(round_serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


@method_decorator(name="post", decorator=swagger_auto_schema(
    request_body=Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "user_id": openapi.Schema(type=openapi.TYPE_INTEGER),
            "round_id": openapi.Schema(type=openapi.TYPE_INTEGER)
        },
        required=["user_id", "round_id"]
    ),
    responses={200: SpinnedRouletteSerializer, 400: "BadRequest", 500: "Internal Server Error"}
))
class SpinRouletteView(APIView):
    """ Spin roulette view """
    def post(self, request, *args, **kwargs):
        user_id = request.data.get("user_id", None)
        round_id = request.data.get("round_id", None)
        if user_id and round_id and user_id != 0 and round_id != 0:
            try:
                spin_status = spin_roulette(user_id, round_id)
            except Exception as e:
                print(e)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            if isinstance(spin_status, dict):
                serializer = SpinnedRouletteSerializer(data=spin_status)
                serializer.is_valid()
                return Response(serializer.data)
            else:
                return Response(status=spin_status)
        return Response(status=status.HTTP_400_BAD_REQUEST)


@method_decorator(name="get", decorator=swagger_auto_schema(
    responses={
        200: Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "online": openapi.Schema(
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            "round_id": openapi.Schema(type=openapi.TYPE_INTEGER),
                            "users_count": openapi.Schema(type=openapi.TYPE_INTEGER)
                        }
                    ),
                    many=True
                )
            }
        ),
        500: "Internal Server Error"
    }
))
class RouletteOnlineStatisticView(APIView):
    """ get roulette online statistic view """
    def get(self, request, *args, **kwargs):
        try:
            online_data = calculate_online_statistic()
        except Exception as e:
            print(e)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        response_data = {"online": online_data}
        return Response(response_data)


@method_decorator(name="get", decorator=swagger_auto_schema(
    responses={
        200: Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "active_users": openapi.Schema(
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            "user_id": openapi.Schema(type=openapi.TYPE_INTEGER),
                            "round_count": openapi.Schema(type=openapi.TYPE_INTEGER),
                            "average_spin_count": openapi.Schema(type=openapi.TYPE_NUMBER)
                        }
                    ),
                    many=True
                )
            }
        ),
        500: "Internal Server Error"
    }
))
class MostActiveUsersView(APIView):
    """ get most active users """
    def get(self, request, *args, **kwargs):
        try:
            users_data = get_most_active_users()
        except Exception as e:
            print(e)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        response_data = {"active_users": users_data}
        return Response(response_data)
